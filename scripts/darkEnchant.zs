// Remove recipes
craftingTable.remove(<item:dark-enchanting:table_upgrade>);
craftingTable.remove(<item:dark-enchanting:dark_enchanter>);


// add new dark e-table upgrade recipe
craftingTable.addShaped("dark-enchanting.table_upgrade", <item:dark-enchanting:table_upgrade>, [[<item:minecraft:emerald_block>, <item:minecraft:gold_block>, <item:minecraft:emerald_block>], [<item:minecraft:gold_block>, <item:minecraft:nether_star>, <item:minecraft:gold_block>], [<item:minecraft:crying_obsidian>, <item:minecraft:crying_obsidian>, <item:minecraft:crying_obsidian>]]);

// add new dark e-table recipe
craftingTable.addShapeless("dark-enchanting.dark_enchanter", <item:dark-enchanting:dark_enchanter>, [<item:minecraft:enchanting_table>, <item:dark-enchanting:table_upgrade>]);
