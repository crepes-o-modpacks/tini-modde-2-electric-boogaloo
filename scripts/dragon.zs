// Import empty ingredients and mirrorAxis
import crafttweaker.api.ingredient.type.IIngredientEmpty;
import crafttweaker.api.recipe.MirrorAxis;

// Remove smithing recipes for armor + tools + sword
smithing.remove(<item:dragonloot:dragon_helmet>);
smithing.remove(<item:dragonloot:dragon_chestplate>);
smithing.remove(<item:dragonloot:dragon_leggings>);
smithing.remove(<item:dragonloot:dragon_boots>);
smithing.remove(<item:dragonloot:dragon_pickaxe>);
smithing.remove(<item:dragonloot:dragon_axe>);
smithing.remove(<item:dragonloot:dragon_hoe>);
smithing.remove(<item:dragonloot:dragon_sword>);


// Add new recipes

// Armor
craftingTable.addShaped("dragonloot.dragon.helmet", <item:dragonloot:dragon_helmet>, [[<item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>, <item:minecraft:netherite_ingot>], [<item:dragonloot:dragon_scale>, IIngredientEmpty.getInstance(), <item:dragonloot:dragon_scale>]]);
craftingTable.addShaped("dragonloot.dragon.chestplate", <item:dragonloot:dragon_chestplate>, [[<item:dragonloot:dragon_scale>, IIngredientEmpty.getInstance(), <item:dragonloot:dragon_scale>], [<item:dragonloot:dragon_scale>, <item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>], [<item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>, <item:minecraft:netherite_ingot>]]);
craftingTable.addShaped("dragonloot.dragon.leggings", <item:dragonloot:dragon_leggings>, [[<item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>, <item:minecraft:netherite_ingot>], [<item:dragonloot:dragon_scale>, IIngredientEmpty.getInstance(), <item:dragonloot:dragon_scale>], [<item:dragonloot:dragon_scale>, IIngredientEmpty.getInstance(), <item:dragonloot:dragon_scale>]]);
craftingTable.addShaped("dragonloot.dragon.boots", <item:dragonloot:dragon_boots>, [[<item:minecraft:netherite_ingot>, IIngredientEmpty.getInstance(), <item:minecraft:netherite_ingot>], [<item:dragonloot:dragon_scale>, IIngredientEmpty.getInstance(), <item:dragonloot:dragon_scale>]]);

// tools/sword
craftingTable.addShaped("dragonloot.dragon.pickaxe", <item:dragonloot:dragon_pickaxe>, [[<item:dragonloot:dragon_scale>, <item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>], [IIngredientEmpty.getInstance(), <item:minecraft:stick>], [IIngredientEmpty.getInstance(), <item:minecraft:stick>]]);
craftingTable.addShapedMirrored("dragonloot.dragon.axe", MirrorAxis.HORIZONTAL, <item:dragonloot:dragon_axe>, [[<item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>, IIngredientEmpty.getInstance()], [<item:dragonloot:dragon_scale>, <item:minecraft:stick>], [IIngredientEmpty.getInstance(), <item:minecraft:stick>]]);
craftingTable.addShapedMirrored("dragonloot.dragon.hoe", MirrorAxis.HORIZONTAL, <item:dragonloot:dragon_hoe>, [[<item:minecraft:netherite_ingot>, <item:dragonloot:dragon_scale>], [<item:minecraft:stick>], [<item:minecraft:stick>]]);
craftingTable.addShaped("dragonloot.dragon.sword", <item:dragonloot:dragon_sword>, [[<item:dragonloot:dragon_scale>], [<item:minecraft:netherite_ingot>], [<item:minecraft:stick>]]);
