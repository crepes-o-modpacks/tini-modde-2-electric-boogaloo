# ARCHIVED

Because this doesn't meet the required standards I set (lagging server), this is archived. I STRONGLY do not recommend you to use this on a server environment. It is, however, quite fine on client.

# Tini Moddé 2 : Electric Boogaloo - The untrying

This is the second edition of my Tini Moddé modpacks. Their main goal is to provide a somewhat good game experience, while keeping the memory usage low and good fps.
It's mainly intended to be used by some friends, so you probably want to go seek another modpack on [Modrinth](https://modrinth.com/modpacks) or [CurseForge](https://www.curseforge.com/minecraft/modpacks).

# Disclaimer

* This modpack is provided as is. No support will be provided. Any action you do is completely on your own.

# Mod credits

As per the licences and requirements :

* [Blast](https://www.curseforge.com/minecraft/mc-mods/blast) - [Github](https://github.com/Ladysnake/BLAST)

* [Effective](https://www.curseforge.com/minecraft/mc-mods/effective) - [Github](https://github.com/ladysnake/effective)

* [Falling leaves](https://modrinth.com/mod/fallingleaves) - [Github](https://github.com/RandomMcSomethin/fallingleaves)

* [Illuminations](https://www.curseforge.com/minecraft/mc-mods/illuminations) - [Github](https://github.com/ladysnake/illuminations)

* [Impaled](https://www.curseforge.com/minecraft/mc-mods/impaled) - [Github](https://github.com/Ladysnake/Impaled)

* [Overweight Farming](https://www.curseforge.com/minecraft/mc-mods/overweight-farming/) - [Github](https://github.com/0rc1nus/Overweight-Farming)

* [Pick Your Poison](https://www.curseforge.com/minecraft/mc-mods/pick-your-poison) - [Github](https://github.com/Ladysnake/Pick-your-poison)

* [Rat's Mischief](https://www.curseforge.com/minecraft/mc-mods/rats-mischief) - [Github](https://github.com/Ladysnake/Rats-Mischief)
